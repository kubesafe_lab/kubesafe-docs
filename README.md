# Kubesafe Documentation

This repository stores the technical documentation for Kubesafe software.
The <a href="https://www.readthedocs.io">readthedocs.io</a> site automatically
builds and publishes from this project.

Kubesafe software runs in a Kubernetes cluster and
* Backs up container-based applications, with all their data
and configuration, to a local or remote destination
* Supports recovery (to repair accidental corruption)
and cloning (for test, development, or analytics)
* Automates application recovery point objectives (RPOs)
with policies
* Is simple to use, requiring no specialized expertise

## Building

To build the documentation in your local directory, install
the python dependencies:

```sh
$ pip3 install -r requirements.txt
```

Build the HTML version of the documentation:

```sh
$ make html
```
