Known Issues
============

This release of Kubesafe software contains some known defects and
limitations:

1. The S3 storage target supports only one producer to one consumer.
   That is, you should only configure two clusters to a particular
   remote S3 target. A future release will remove this limitation.

2. S3 backups are full rather than incremental. A future release will
   back up only changed data to S3.

3. Remote protection policies do not respect the retention value. That
   is, snapshots created on the remote target remain indefinitely. You
   must remove snapshots when you no longer need them. A future release
   will prune remote snapshots automatically to keep only the specified
   number.

4. If you back up an application that is not in a runnable condition,
   future clones or restores from that snapshot will also not be in a
   runnable condition.

5. You can't delete cluster or object store
   configurations once they have been uploaded to Kubesafe. A future
   release will add this capability.

6. The cluster name (ks_cluster_id) you specify in configuration files
   must not contain any of these characters:

..

   /\. "$

7. If you define an application as an individual Pod, Kubesafe cannot
   restore or clone the application on a remote cluster. Such
   configurations are unusual, as applications will normally use a
   Deployment, StatefulSet, or DaemonSet rather than defining a Pod
   directly.

8. Kubesafe requires that all nodes in your cluster run in the same
   region and zone.

9. Kubesafe currently supports only a single type of storage on
   clusters used for cloning or restoring applications. For example,
   a cluster used for cloning or restoring can use EBS storage
   or Gluster storage, but not both.
