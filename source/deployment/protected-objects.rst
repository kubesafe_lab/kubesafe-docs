Protected Objects
=================

Kubesafe software includes these Kubernetes objects in each
recovery point:

-  configmaps
-  cronjobs.batch
-  daemonsets.apps
-  deployments.apps
-  endpoints
-  imagepullsecrets
-  jobs.batch
-  persistentvolumeclaims
-  pods
-  replicasets.apps
-  replicationcontrollers
-  resourcequotas
-  secrets
-  services
-  statefulsets.apps

Applications might include other objects, which are not currently included
in Kubesafe recovery points:

- daemonsets.extensions
- deployments.extensions
- ingress
- podtemplates
- replicasets.extensions
- rolebindings.rbac.authorization.k8s.io
- roles.rbac.authorization.k8s.io
- serviceaccounts

