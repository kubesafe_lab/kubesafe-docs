Collecting Log Details
======================

If you encounter problems and need to send log details back to Kubesafe,
run the following commands to generate a file you can attach to email::

  kubectl logs -n kubesafe \
    $(kubectl get pods -n kubesafe -o name -lapp=kubesafe-api)

