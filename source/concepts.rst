Kubesafe Concepts
=========================

This documents talks about Kubesafe concepts: what the software does
and how to accomplish key use cases.

Use Cases
-----------

.. image:: _static/use-cases.png
  :alt: Kubesafe use cases

Terminology
------------
Recovery Point
  A point in time copy of a namespace or application, including its data
  and configuration. A recovery point may be local (visible on the same
  cluster where the aplication is running) or remote (visible on a cluster
  or object store other than the one where the cluster is running).

Back up
  (verb) Create a new recovery point.

Backup
  (nound) A recovery point.

Snapshot
  (noun) A recovery point.

Restore
  Replace the contents of a namespace or application with the data and
  configuration from a recovery point. Because restores are done in
  place, this action is possible only when the recovery point exists
  on the cluster where the application is running.

Restore Latest
  Restore from the most recent recovery point for a namespace or application.

Clone
  Create a new running copy of a namespace or aplication using the
  data and configuration from a recovery point.

Clone Latest
  Clone from the most recent recovery point for a namespace or application.

Protectable
  Describes a namespace or application running on the current cluster.

Protected
  Describes a namespace or application with at least one recovery point.

Recoverable
  Describes a namespace or application having one or more recovery points
  on the current cluster.


