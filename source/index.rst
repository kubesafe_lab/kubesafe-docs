
Kubesafe Documentation
=======================

What's New
----------

* December, 2020: Now supports storage hosted on NetApp ONTAP systems, including
  Cloud Volumes ONTAP.

.. toctree::
   :maxdepth: 1
   :caption: Contents

   deployment
   concepts

