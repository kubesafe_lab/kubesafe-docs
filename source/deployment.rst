Kubesafe Deployment Guide
=========================

Kubesafe software runs on each of your clusters, allowing you to back up, recover, and clone
applications or namespaces.

This version supports

.. list-table::
   :widths: 40 20 20 20
   :header-rows: 1

   * - Container Orchestrator
     - Server Infrastructure
     - Metadata Repository
     - Storage
   * - Kubernetes
     - AWS
     - S3
     - EBS, NetApp ONTAP
   * - OpenShift Container Platform (OCP)
     - AWS
     - S3
     - EBS
   * - OpenShift Container Platform (OCP)
     - VMware
     - S3
     - Gluster


.. toctree::
   :maxdepth: 2

   deployment/aws
   deployment/gluster
   deployment/logs
   deployment/protected-objects
   deployment/issues

